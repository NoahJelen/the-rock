use std::{env, fs};
use lazy_static::lazy_static;
use rust_utils::{logging::Log, utils};

mod tui;
mod cli;
mod bible;
mod config;
mod obi_search;

lazy_static! {
    pub static ref LOG: Log = Log::new(&utils::get_execname(), "the_rock");
    pub static ref CACHE_DIR: String = env::var("XDG_CACHE_HOME").unwrap_or(format!("{}/.cache", env::var("HOME").unwrap()));
}

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    LOG.report_panics(true);

    // create the cache folder if it doesn't exist
    fs::create_dir_all(&*CACHE_DIR).unwrap_or_default();

    let name = utils::get_execname();

    // get the command line arguments of the program
    let mut args = env::args();
    args.next();

    if name == "bible" {
        cli::parse_args(args);
    }

    // start the TUI
    else if name == "the_rock" {
        tui::init(args);
    }
}