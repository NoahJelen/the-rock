use std::env::Args;
use colored::Colorize;
use regex::RegexBuilder;

use crate::{
    VERSION,
    bible::{Format, search_word, VerseRef},
    obi_search
};

pub fn parse_args(mut args: Args) {
    if let Some(arg) = args.next() {
        let verses = args.next().unwrap_or("Ge1:1".to_string());

        match arg.as_str() {
            "-v" => println!("The Rock: A King James Bible Viewer\nVersion {VERSION}"),
            "-h" => show_help(),
            "-c" => parse_refs(&verses, false, true),
            "-p" => parse_refs(&verses, true, false),
            "-pc" => parse_refs(&verses, true, true),
            "-cp" => parse_refs(&verses, true, true),
            "-s" => find_term(&verses),
            "-t" => topic_search(&verses),
            _ => parse_refs(&arg, false, false)
        }
    }
    else {
        parse_refs("Ge1:1", false, false);
    }
}

fn topic_search(topic: &str) {
    match obi_search::get_topic(topic) {
        Ok(results) => {
            let title = format!("What the Bible says about {topic}:\n").bright_white();
            println!("{title}");
            for verse in results {
                if let Some(verse_data) = VerseRef::parse(verse) {
                    println!("{}{verse_data}", verse_data.title().bright_white())
                }
            }
            println!("\nPowered by openbible.info");
        }

        Err(why) => {
            eprintln!("{}{}{}{}", "Error getting topic \"".red(), topic.red(), "\":", why.to_string().red())
        }
    }
}

fn find_term(term: &str) {
    let regex = RegexBuilder::new(term)
        .case_insensitive(true)
        .build()
        .expect("Invalid Regex!");

    let results = search_word(&regex);
    if results.is_empty() {
        println!("The term \"{term}\" did not return any results!");
        return;
    }

    for verse in results {
        let verse_text = verse.to_string();
        print!("{} ", verse.title().bright_white());
        let mut locations = Vec::new();
        for rmatch in regex.find_iter(&verse_text) {
            locations.push((rmatch.start(), rmatch.end()));
        }
        for (i, c) in verse_text.chars().enumerate() {
            let mut colored_char = false;
            for (start, end) in locations.iter().copied() {
                if (start..end).contains(&i) {
                    print!("{}", c.to_string().blue());
                    colored_char = true;
                    continue;
                }
            }
            if !colored_char {
                print!("{c}");
            }
        }
        println!()
    }
}

// parse verse and passage references and output them
fn parse_refs(verses: &str, isparagraph: bool, entire_chapter: bool) {
    let format = if isparagraph {
        Format::Paragraph
    }
    else {
        Format::Numbers
    };
    let verse_list = verses.split(',');
    for verse_ref in verse_list {
        if let Some(verse) = VerseRef::parse(verse_ref) {
            let verse = verse.format(format);
            if entire_chapter {
                let chapter = verse.chapter();
                let title = chapter.title().bright_white();
                if chapter.format == Format::Numbers { println!("{title}\n\n{chapter}"); }
                else { println!("{title}: {chapter}"); }
                continue;
            }
            print!("{}", verse.title().bright_white());
            println!("{verse}");
        }
    }   
}

// show the help for the program
fn show_help() {
    println!("{}", "Usage:".bright_white());
    println!("bible <verse>");
    println!("bible <passage>");
    println!("bible <verse or passage>[,verse or passage,verse or passage...]\n");

    println!("{}\n", "Examples:".bright_white());
    println!("bible Mark9:23");
    println!("bible matt24:4-14");
    println!("bible isa40:31,matt6:9-13,Genesis1\n");

    println!("Use -v to show current version.");
    println!("Use -h to show this help message again.");
    println!("Use -p <passage> to show the passage as a paragraph instead of verse list");
    println!("Use -c <book number><chapter number> to show an entire chapter");
    println!("Use -s <word> to find all instances of a specific word.");
    println!("Use -t <topic> to search for a topic on openbible.info\n");

    println!("{} <book name><chapter>:<verse>[-<ending verse if passage>]", "Passage and verse syntax:".bright_white());
}