use rust_utils::config;

use crate::bible::Format;

#[derive(Clone)]
#[config(file_name = "config.toml", save_dir = "the_rock")]
pub struct BibleConfig {
    // the format of the verses (numbered list, paragraph, or paragraph with numbers?)
    pub format: Format,

    // bookmarked passages/verses (by default, it is Matthew6:9-13, John3:16, Mark9:23, and Exodus 20)
    pub bookmarks: Vec<(String,String)>,
    
    // should parables be shown?
    pub show_parables: bool,
    
    // should bookmarks be shown?
    pub show_bookmarks: bool
}

impl Default for BibleConfig {
    // returns the default settings
    fn default() -> BibleConfig {
        BibleConfig {
            format: Format::Numbers,

            bookmarks: [
                ("The Lord's Prayer", "Matt6:9-13"),
                ("Eternal Promise", "John3:16"),
                ("Everything is Possible", "Mark9:23"),
                ("The 10 Commandments", "Exod20:1-26"),
                ("Israel Prophecy around 537BC", "Eze37:10-14")
            ]
                .into_iter()
                .map(|(name, verse)| (name.to_string(), verse.to_string()))
                .collect(),

            show_parables: true,
            show_bookmarks: true,
        }
    }
}