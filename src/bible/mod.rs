use std::{
    env,
    fs,
    fmt::{Display, Formatter, Result as FmtResult}
};
use cursive::{
    theme::{BaseColor, Color, ColorStyle, Style},
    utils::markup::StyledString
};
use regex::Regex;
use rust_utils::config::Config;
use serde::{Deserialize, Serialize};
use crate::config::BibleConfig;

pub use self::{
    books::{Book, BOOKS},
    chapter::Chapter
};

mod chapter;
mod books;

#[derive(Clone, Copy, PartialEq, Eq, Deserialize, Serialize)]
pub enum Format {
    // show passages with verse numbers
    Numbers,

    // show the passages as paragraphs
    Paragraph,

    // show as a paragraph, but with verse numbers before each verse
    NumParagraph
}

impl Format {
    pub fn next(&self) -> Self {
        match *self {
            Format::Numbers => Format::Paragraph,
            Format::Paragraph => Format::NumParagraph,
            Format::NumParagraph => Format::Numbers
        }
    }
}

impl Display for Format {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match *self {
            Format::Numbers => write!(f, "Numbered List"),
            Format::Paragraph => write!(f, "Paragraph"),
            Format::NumParagraph => write!(f, "Paragraph with verse numbers")
        }
    }
}

// reference to a verse or passage
#[derive(Clone, Copy)]
pub struct VerseRef {
    pub book: Book, // book
    pub chapter: usize, // chapter number
    pub verse: usize, // verse number
    pub last_verse: usize, // ending verse (if passage)
    pub ispassage: bool, // is this a passage
    format: Format // output format
}

impl VerseRef {
    // parses a verse reference string
    pub fn parse<V: Display>(verse_in: V) -> Option<Self> {
        let verse = verse_in.to_string();
        let mut ispassage = false;
        let mut book_raw: String = String::new();
        let mut last_verse_num = 0;

        // parse the book name
        for (counter, c) in verse.chars().enumerate() {
            if counter == 0 || !c.is_ascii_digit() {
                book_raw.push(c);
            }
        }

        // if there is a '-' char in the string, assume this is a passage
        let dashloc = verse.find('-').unwrap_or(verse.len());

        // parse the chapter number
        // if there isn't one, use 1 as the chapter number
        let mut colonloc = verse.find(':').unwrap_or(verse.len());

        let chapter_num = if colonloc == verse.len() {
            verse.get(book_raw.len()..verse.len())?.parse().unwrap_or(1)
        }
        else if dashloc < verse.len() {
            verse.get(book_raw.len() - 2..colonloc)?.parse().ok()?
        }
        else {
            verse.get(book_raw.len() - 1..colonloc)?.parse().ok()?
        };

        // parse the verse number
        // if there isn't one, use 1 as the verse number
        colonloc = verse.find(':').unwrap_or(verse.len());
        let verse_num = if colonloc == dashloc {
            1
        }
        else { verse.get(colonloc + 1..dashloc)?.parse().ok()? };

        if book_raw.contains(':') {
            book_raw.pop();
        }

        // if this is a passage, get the number of the last verse
        if dashloc < verse.len() {
            ispassage = true;
            last_verse_num = verse.get(dashloc + 1..verse.len())?.parse().ok()?;
        }

        let book = if let Some(b) = Book::by_name(&book_raw) { b }
        else {
            book_raw = book_raw.get(..book_raw.len() - 1)?.to_string();
            Book::by_name(&book_raw)?
        };

        let format = if ispassage {
            BibleConfig::load().format
        }
        else {
            Format::Paragraph
        };

        if book_raw.ends_with(':') {
            book_raw.pop();
        }

        Some(VerseRef {
            book,
            chapter: chapter_num,
            verse: verse_num,
            last_verse: last_verse_num,
            ispassage,
            format
        })
    }

    pub fn format(mut self, format: Format) -> Self {
        self.format = format;
        self
    }

    // gets the formatted title
    pub fn title(&self) -> String {
        if self.ispassage {
            match self.format {
                Format::Paragraph | Format::NumParagraph => format!("{} {}:{}-{}: ", self.book, self.chapter, self.verse, self.last_verse),
                Format::Numbers => format!("{} {}\n\n", self.book, self.chapter)
            }
        }
        else {
            format!("{} {}:{}: ", self.book, self.chapter, self.verse)
        }
    }

    pub fn chapter(&self) -> Chapter { self.book.chapter(self.chapter) }

    pub fn content_with_results(&self, regex: &Regex) -> StyledString {
        let text = self.to_string();
        result_string(regex, &text, true)
    }
}

// show the verse text when it is displayed
impl Display for VerseRef {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        let chapter = self.chapter();
        if self.ispassage {
            let passage = &chapter[self.verse - 1..self.last_verse];
            for (i, v) in passage.iter().enumerate() {
                match self.format {
                    Format::Numbers => writeln!(f, "{} {v}", self.verse + i)?,
                    Format::Paragraph => write!(f, "{v} ")?,
                    Format::NumParagraph => write!(f, "{} {v} ", self.verse + i)?
                }
            }

            Ok(())
        }
        else { write!(f, "{}", &chapter[self.verse]) }
    }
}

impl From<VerseRef> for String {
    fn from(vref: VerseRef) -> String {
        vref.to_string()
    }
}

// finds a specific term in the bible
// returns a vector of verse refs containing the term
pub fn search_word(regex: &Regex) -> Vec<VerseRef> {
    let bible = open_bible();
    let mut results = Vec::new();
    for verse in bible {
        if regex.is_match(&verse) {
            let verse_ref: &str = verse.split(' ').next().unwrap();
            if let Some(verse) = VerseRef::parse(verse_ref) {
                results.push(verse);
            }
        }
    }

    results
}

// returns every verse from the bible.rawtext file as a vector of strings
fn open_bible() -> Vec<String> {
    let mut bible_raw = Vec::new();
    let file =
        if let Ok(file) = fs::read_to_string("/usr/share/the_rock/bible.rawtext") { file }
        else if let Ok(file) = fs::read_to_string("bible.rawtext") { file }
        else {
            fs::read_to_string(format!("{}/share/the_rock/bible.rawtext", env::var("PREFIX").expect("Not a Termux environment!"))).unwrap()
        };

    for (count, line) in file.lines().enumerate() {
        // skip the first line (it is not part of the actual text)
        if count > 0 {
            bible_raw.push(String::from(line));
        }
    }

    bible_raw
}

fn result_string(regex: &Regex, text: &str, dark: bool) -> StyledString {
    let mut content = StyledString::new();
    let style = if dark {
        Style::from(Color::Dark(BaseColor::White))
    }
    else { Style::default() };

    let mut locations = Vec::new();
    for rmatch in regex.find_iter(text) {
        locations.push((rmatch.start(), rmatch.end()));
    }

    if locations.is_empty() {
        content.append_styled(text, style)
    }
    else {
        for (i, c) in text.chars().enumerate() {
            let mut colored_char = false;
            for location in &locations {
                if (location.0..location.1).contains(&i) {
                    content.append_styled(c, Style::from(Color::Light(BaseColor::Blue)));
                    colored_char = true;
                    continue;
                }
            }
            if !colored_char {
                content.append_styled(c, style)
            }
        }
    }

    content
}

// the parables of Jesus christ
// first tuple element is the name and the second is the verse reference
pub static PARABLES: [(&str, &str); 61]  = [
    ("New Cloth on an Old Coat 1", "Mat9:16"),
    ("New Cloth on an Old Coat 2", "Mark2:21"),
    ("New Cloth on an Old Coat 3", "Luke5:36"),

    ("New Wine in Old Wineskins 1", "Mark9:17"),
    ("New Wine in Old Wineskins 2", "Mark2:22"),
    ("New Wine in Old Wineskins 3", "Luke5:37-38"),

    ("The Lamp on a Stand 1", "Mat5:14-15"),
    ("The Lamp on a Stand 2", "Mark4:21-22"),
    ("The Lamp on a Stand 3", "Luke8:16"),
    ("The Lamp on a Stand 4", "Luke11:33"),

    ("The Wise and Foolish Builders 1", "Mat7:24-27"),
    ("The Wise and Foolish Builders 2", "Luke6:47-49"),

    ("The Moneylender forgiving unequal debts", "Luke7:41-43"),
    ("The Rich Fool", "Luke12:16-21"),

    ("The Servants Must Remain Watchful 1", "Mark13:35-37"),
    ("The Servants Must Remain Watchful 2", "Luke12:35-40"),

    ("The Wise and Foolish Servants 1", "Mat24:45-51"),
    ("The Wise and Foolish Servants 2", "Luke12:42-48"),

    ("The Unfruitful Fig Tree", "Luke13:6-9"),

    ("The Parable of the Soils 1", "Mat13:3-23"),
    ("The Parable of the Soils 2", "Mark4:1-20"),
    ("The Parable of the Soils 3", "Luke8:4-15"),

    ("The Weeds Among Good Plants", "Mat13:24-43"),
    ("The Growing Seed", "Mark4:26-29"),

    ("The Mustard Seed 1", "Mat13:31-32"),
    ("The Mustard Seed 2", "Mark4:30-32"),
    ("The Mustard Seed 3", "Luke13:18-19"),

    ("Yeast", "Mat13:31-32"),
    ("Hidden Treasure", "Mat13:44"),
    ("Valuable Pearl", "Mat13:45-46"),
    ("Fishing Net", "Mat13:47-50"),
    ("Owner of a House", "Mat13:52"),
    ("Lost Sheep", "Mat18:12-14"),
    ("The Master and His Servant", "Luke17:7-10"),
    ("The Unmerciful servant", "Mat18:23-34"),
    ("The Good Samaritan", "Luke10:30-37"),
    ("Friend in Need", "Luke11:5-8"),
    ("Lowest Seat at the Feast", "Luke14:7-14"),
    ("Invitation to a Great Banquet", "Luke14:16-24"),
    ("The Cost of Discipleship", "Luke14:28-33"),
    ("Lost Sheep", "Luke15:4-7"),
    ("Lost Coin", "Luke15:8-10"),
    ("The Prodigal Son", "Luke15:11-32"),
    ("The Shrewd Manager", "Luke16:1-8"),
    ("The Rich Man and Lazarus", "Luke16:19-31"),
    ("The Early and Late Workers in the Vineyard", "Mat20:1-16"),
    ("The Persistent Widow and Crooked Judge", "Mat18:1-8"),
    ("The Pharisee and Tax Collector", "Luke18:10-14"),
    ("The King's Ten Servants Given Minas", "Luke19:12-27"),
    ("Two Sons (one obeys, one disobeys)", "Mat21:28-32"),

    ("Wicked Tenants 1", "Mat21:33-44"),
    ("Wicked Tenants 2", "Mark12:1-11"),
    ("Wicked Tenants 3", "Luke20:9-18"),

    ("Invitation to a Wedding Banquet", "Mat22:2-14"),

    ("The Fig Tree and Signs of the Future 1", "Mat24:32-35"),
    ("The Fig Tree and Signs of the Future 2", "Mark13:28-29"),
    ("The Fig Tree and Signs of the Future 3", "Luke21:29-31"),

    ("The Wise and Foolish Virgins", "Mat25:1-13"),
    ("The Talents", "Mat25:14-30"),
    ("The Sheep and the Goats", "Mat25:31-46"),
    ("The Sheep, Shepherd, and Gate", "John10:1-18")
];