use rust_utils::utils;
use serde::{Deserialize, Serialize};
use super::*;

macro_rules! book {
    ($num:expr, $name:expr, $short1:expr, $short2:expr, $chapters:expr) => {
        Book {
            num: $num,
            name: $name,
            short1: $short1,
            short2: $short2,
            chapters: $chapters
        }
    };

    ($num:expr, $name:expr) => { book!($num, $name, $name, $name, 1) };
    ($num:expr, $name:expr, $short1:expr, $short2:expr) => { book!($num, $name, $short1, $short2, 1) };
    ($num:expr, $name:expr, $chapters:expr) => { book!($num, $name, $name, $name, $chapters) };
}

#[derive(Copy, Clone, Serialize, Deserialize)]
pub struct Book {
    pub num: usize,

    #[serde(skip)]
    pub name: &'static str,

    #[serde(skip)]
    pub short1: &'static str,

    #[serde(skip)]
    pub short2: &'static str,

    pub chapters: usize
}

impl Book {
    pub fn by_name<N: Display>(name: N) -> Option<Book> {
        // capitalize the first letter, if needed
        let mut namecap = utils::capitalize(name);
        if namecap == "Psalm" {
            namecap.push('s');
        }

        BOOKS.into_iter().find(|&book| namecap == book.name || namecap == book.short1 || namecap == book.short2)
    }

    // this book just have 1 chapter?
    pub fn single_chapter(self) -> bool { self.chapters == 1 }

    // get a chapter from this book
    pub fn chapter(self, num: usize) -> Chapter {
        Chapter {
            book: self,
            num,
            format: BibleConfig::load().format,
            content: self.raw_chapter(num)
        }
    }

    // get the text of a chapter in this book as a list of strings
    pub (super) fn raw_chapter(self, chapter_num: usize) -> Vec<String> {
        let mut content = Vec::new();
        let book_text = self.raw_text();
        let chapter_name = format!("{}{chapter_num}:", self.short1);

        for line in book_text {
            let verse = line.split_whitespace().next().unwrap();
            let verse_start = verse.chars().count() + 1;
            if verse.starts_with(&chapter_name) {
                content.push(line[verse_start..].to_string())
            }
        }

        content
    }

    // return every verse from a specific book
    // panics if the book number is greater than 65 (the books start at 0)
    fn raw_text(self) -> Vec<String> {
        let bible_raw = open_bible();
        let mut book_text = Vec::new(); // book as vector of strings (the verses)

        for line in bible_raw {
            let verse = line.split_whitespace().next().unwrap();
            if verse.starts_with(self.short1) {
                book_text.push(line)
            }
        }

        book_text
    }
}

impl Display for Book {
    fn fmt(&self, f: &mut Formatter) -> FmtResult { write!(f, "{}", self.name) }
}

pub static BOOKS: [Book; 66] = [
    book!(0, "Genesis", "Ge", "Gen", 50),
    book!(1, "Exodus", "Exo", "Exod", 40),
    book!(2, "Leviticus", "Lev", "Lev", 27),
    book!(3, "Numbers", "Num", "Num", 36),
    book!(4, "Deuteronomy", "Deu", "Deut", 34),
    book!(5, "Joshua", "Josh", "Josh", 24),
    book!(6, "Judges", "Jdgs", "Judg", 21),
    book!(7, "Ruth", 4),
    book!(8, "1Samuel", "1Sm", "1Sam", 31),
    book!(9, "2Samuel", "2Sm", "2Sam", 24),
    book!(10, "1Kings", "1Ki", "1Kgs", 22),
    book!(11, "2Kings", "2Ki", "2Kgs", 35),
    book!(12, "1Chronicles", "1Chr", "1Chr", 29),
    book!(13, "2Chronicles", "2Chr", "2Chr", 36),
    book!(14, "Ezra", 10),
    book!(15, "Nehemiah", "Neh", "Neh", 13),
    book!(16, "Esther", "Est", "Esth", 10),
    book!(17, "Job", 42),
    book!(18, "Psalms", "Psa", "Ps", 150),
    book!(19, "Proverbs", "Prv", "Prov", 31),
    book!(20, "Ecclesiastes", "Eccl", "Eccl", 12),
    book!(21, "Song", "SSol", "Song", 8),
    book!(22, "Isaiah", "Isa", "Isa", 66),
    book!(23, "Jeremiah", "Jer", "Jer", 52),
    book!(24, "Lamentations", "Lam", "Lam", 5),
    book!(25, "Ezekiel", "Eze", "Ezek", 48),
    book!(26, "Daniel", "Dan", "Dan", 12),
    book!(27, "Hosea", "Hos", "Hos", 14),
    book!(28, "Joel", 3),
    book!(29, "Amos", 9),
    book!(30, "Obadiah", "Obad", "Obad"),
    book!(31, "Jonah", "Jonah", "Jonah", 4),
    book!(32, "Micah", "Mic", "Mic", 7),
    book!(33, "Nahum", "Nahum", "Nah", 3),
    book!(34, "Habakkuk", "Hab", "Hab", 3),
    book!(35, "Zephaniah", "Zep", "Zeph", 3),
    book!(36, "Haggai", "Hag", "Hag", 2),
    book!(37, "Zechariah", "Zec", "Zech", 14),
    book!(38, "Malachi", "Mal", "Mal", 4),
    book!(39, "Matthew", "Mat", "Matt", 28),
    book!(40, "Mark", 16),
    book!(41, "Luke", 24),
    book!(42, "John", 21),
    book!(43, "Acts", 28),
    book!(44, "Romans", "Rom", "Rom", 16),
    book!(45, "1Corinthians", "1Cor", "1Cor", 16),
    book!(46, "2Corinthians", "2Cor", "2Cor", 13),
    book!(47, "Galatians", "Gal", "Gal", 6),
    book!(48, "Ephesians", "Eph", "Eph", 6),
    book!(49, "Philippians", "Phi", "Phil", 4),
    book!(50, "Colossians", "Col", "Col", 4),
    book!(51, "1Thessalonians", "1Th", "1Thess", 5),
    book!(52, "2Thessalonians", "2Th", "2Thess", 3),
    book!(53, "1Timothy", "1Tim", "1Tim", 6),
    book!(54, "2Timothy", "2Tim", "2Tim", 4),
    book!(55, "Titus", 3),
    book!(56, "Philemon", "Phmn", "Phlm"),
    book!(57, "Hebrews", "Heb", "Heb", 13),
    book!(58, "James", "Jas", "Jas", 5),
    book!(59, "1Peter", "1Pet", "1Pet", 5),
    book!(60, "2Peter", "2Pet", "2Pet", 3),
    book!(61, "1John", "1Jn", "1John", 5),
    book!(62, "2John", "2Jn", "2John"),
    book!(63, "3John", "3Jn", "3John"),
    book!(64, "Jude"),
    book!(65, "Revelation", "Rev", "Rev", 22)
];