use std::ops::{
    Index,
    Range,
    RangeFrom,
    RangeFull,
    RangeTo,
};

use regex::Regex;
use rust_utils::config::Config;
use serde::{Deserialize, Serialize};
use crate::config::BibleConfig;

use super::*;

// content type for the chapter
enum ContentType<'a> {
    // contains the topic name and selected verse
    Topic(&'a str, &'a VerseRef),

    // text search result
    SearchResult(Regex),

    None
}

impl<'a> ContentType<'a> {
    fn is_none(&self) -> bool { matches!(self, Self::None) }
}

// object for chapter info
// used in the TUI
#[derive(Clone, Serialize, Deserialize)]
pub struct Chapter {
    pub book: Book,
    pub num: usize,
    pub format: Format,

    #[serde(skip)]
    pub (super) content: Vec<String>
}

impl Chapter {
    // go to the next chapter
    pub fn next(&mut self) {
        // return to Genesis 1 if we're at Revelation 22
        if self.book.num == 65 && self.num == 22 {
            self.num = 1;
            self.book = BOOKS[0];
        }

        // go to the next book if we're at the end
        else if self.num + 1 > self.book.chapters {
            self.num = 1;
            let new_book_num = self.book.num + 1;
            self.book = BOOKS[new_book_num];
        }

        // increment the chapter number
        else { self.num += 1; }
        self.content = self.book.raw_chapter(self.num);
    }

    // go to the next chapter
    pub fn prev(&mut self) {
        // go to Revelation 22 if we're at Genesis 1
        if self.num == 1 && self.book.num == 0 {
            self.num = 22;
            self.book = BOOKS[65];
        }
        // go to the previous book if we're at the beginning
        else if self.num == 1 && self.book.num > 0 {
            let new_book_num = self.book.num - 1;
            self.book = BOOKS[new_book_num];
            self.num = self.book.chapters;
        }
        // decrement the chapter number
        else { self.num -= 1; }
        self.content = self.book.raw_chapter(self.num);
    }

    // get the title of the chapter
    pub fn title(&self) -> String {
        if self.book.single_chapter() {
            format!("{}", self.book)
        }
        else {
            format!("{} {}", self.book, self.num)
        }
    }

    // get the chapter content
    #[inline]
    pub fn content(&self) -> StyledString { self.content_inner(ContentType::None) }

    #[inline]
    pub fn content_with_topic(&self, topic: &str, topic_verse: &VerseRef) -> StyledString { self.content_inner(ContentType::Topic(topic, topic_verse)) }

    #[inline]
    pub fn content_with_results(&self, regex: Regex) -> StyledString { self.content_inner(ContentType::SearchResult(regex)) }

    // when the chapter is loaded from disk
    // the content is empty
    // this loads the chapter content
    pub fn load_content(&mut self) { self.reformat(self.format); }

    // reformats the chapter text when the format is changed
    pub fn reformat(&mut self, new_format: Format) {
        // reload the content
        self.content = self.book.raw_chapter(self.num);
        self.format = new_format;

        // reload book ref
        let book_num = self.book.num;
        self.book = BOOKS[book_num];
    }

    fn content_inner(&self, content_type: ContentType) -> StyledString {
        let mut content = StyledString::new();
        let mut has_topic = false;
        if content_type.is_none() {
            content = self.get_content_str();
        }
        else {
            for (verse_num, verse) in self.content.iter().enumerate() {
                let line = match self.format {
                    Format::Paragraph => format!("{verse} "),
                    Format::Numbers => format!("{} {verse}\n", verse_num + 1),
                    Format::NumParagraph => format!("{} {verse} ", verse_num + 1)
                };

                match content_type {
                    ContentType::SearchResult(ref regex) => content.append(result_string(regex, &line, false)),

                    ContentType::Topic(topic, t_verse) if verse_num + 1 == t_verse.verse || (t_verse.ispassage && (verse_num + 1 >= t_verse.verse && verse_num < t_verse.last_verse)) => {
                        let topic_verse = VerseRef {
                            format: self.format,
                            ..*t_verse
                        };
                        let topic_style = Style::from(Color::from_256colors(165));
                        let topic_title_style = ColorStyle::new(Color::Light(BaseColor::White),Color::from_256colors(165));

                        if !has_topic {
                            content.append_styled(format!("Topic: {topic}"), topic_title_style);
                            content.append(
                                match self.format {
                                    Format::Paragraph | Format::NumParagraph => " ",
                                    Format::Numbers => "\n"
                                }
                            );
                            if topic_verse.ispassage { content.append_styled(topic_verse, topic_style); }
                            else { content.append_styled(&line, topic_style); }
                            has_topic = true;
                        }
                    }

                    _ => content.append(line)
                }
            }
        }

        content
    }

    fn get_content_str(&self) -> StyledString {
        let settings = BibleConfig::load();
        let mut new_content = StyledString::new();
        let parable_style = Style::from(Color::Light(BaseColor::Red));
        let bookmark_style = Style::from(Color::Light(BaseColor::Blue));
        let parable_title_style = ColorStyle::new(Color::Light(BaseColor::White), Color::Dark(BaseColor::Red));
        let bookmark_title_style = ColorStyle::new(Color::Light(BaseColor::White), Color::Dark(BaseColor::Blue));
        let mut prev_parable_title = String::new();
        let mut prev_bookmark_title = String::new();
        for (verse_num, verse) in self.content.iter().enumerate() {
            let line = match self.format {
                Format::Paragraph => format!(" {verse}"),
                Format::Numbers => format!("{} {verse}\n", verse_num + 1),
                Format::NumParagraph => format!("{} {verse} ", verse_num + 1)
            };

            if let Some(bookmark_title) = bookmark_title(self.book.num, self.num, verse_num + 1) {
                if settings.show_bookmarks {
                    if bookmark_title != prev_bookmark_title && self.format == Format::Numbers {
                        new_content.append_styled(&bookmark_title, bookmark_title_style);
                    }
                    new_content.append_styled(&line, bookmark_style);
                    prev_bookmark_title = bookmark_title;
                    continue;
                }
                else { new_content.append(line); }
            }
            else if let Some(parable) = parable_title(self.book.num, self.num, verse_num + 1) {
                if settings.show_parables {
                    if parable != prev_parable_title && self.format ==  Format::Numbers {
                        new_content.append_styled(&parable, parable_title_style);
                    }

                    new_content.append_styled(&line, parable_style);
                    prev_parable_title = parable.clone();
                    continue;
                }
                else { new_content.append(&line); }
            }
            else { new_content.append(&line) }
        }

        new_content
    }
}

impl Display for Chapter {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        for (verse_num, line) in self.content.iter().enumerate() {
            match self.format {
                Format::Paragraph => write!(f, " {line}")?,
                Format::Numbers => writeln!(f, "{} {line}", verse_num + 1)?,
                Format::NumParagraph => write!(f, "{} {line} ", verse_num + 1)?
            };
        }
        Ok(())
    }
}

impl Index<RangeTo<usize>> for Chapter {
    type Output = [String];

    #[inline]
    fn index(&self, index: RangeTo<usize>) -> &[String] { &self.content[index] }
}

impl Index<Range<usize>> for Chapter {
    type Output = [String];

    #[inline]
    fn index(&self, index: Range<usize>) -> &[String] {
        if index.end >= self.content.len() { &self.content[index.start..] }
        else { &self.content[index] }
    }
}

impl Index<RangeFrom<usize>> for Chapter {
    type Output = [String];

    #[inline]
    fn index(&self, index: RangeFrom<usize>) -> &[String] { &self.content[index] }
}

impl Index<RangeFull> for Chapter {
    type Output = [String];

    #[inline]
    fn index(&self, index: RangeFull) -> &[String] { &self.content[index] }
}

impl Index<usize> for Chapter {
    type Output = str;

    #[inline]
    fn index(&self, index: usize) -> &str { &self.content[index - 1] }
}

// get bookmark title (if available)
fn bookmark_title(book_num: usize, chapter_num: usize, verse_num: usize) -> Option<String> {
    let bookmarks = BibleConfig::load().bookmarks;
    let verse = (BOOKS.get(book_num)?.name, chapter_num, verse_num);

    for (title, verse_ref) in bookmarks {
        if let Some(v) = VerseRef::parse(verse_ref) {
            if v.ispassage {
                for cur_verse in v.verse..=v.last_verse {
                    if verse == (v.book.name, v.chapter, cur_verse) {
                        return Some(format!("{title}\n"));
                    }
                }
            }
            else if verse == (v.book.name, v.chapter, v.verse) {
                return Some(format!("{title}\n"));
            }
        }
    }
    None
}

// get parable title (if available)
fn parable_title(book_num: usize, chapter_num: usize, verse_num: usize) -> Option<String> {
    // the parables are only in the gospels
    // if this isn't a gospel book, don't bother looking for parables
    if !(39..=42).contains(&book_num) {
        return None;
    }

    let verse = (BOOKS[book_num].name, chapter_num, verse_num);
    for (title, verse_ref) in PARABLES {
        let parable = VerseRef::parse(verse_ref).unwrap();
        if parable.ispassage {
            for cur_verse in parable.verse..=parable.last_verse {
                if verse == (parable.book.name, parable.chapter, cur_verse) {
                    return Some(format!("{title}\n"));
                }
            }
        }
        else if verse == (parable.book.name, parable.chapter, parable.verse) {
            return Some(format!("{title}\n"));
        }
    }

    None
}