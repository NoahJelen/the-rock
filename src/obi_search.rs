use crate::CACHE_DIR;
use std::{
    fmt::Write,
    fs
};
use lazy_static::lazy_static;
use regex::Regex;
use reqwest::{blocking, Result as WebResult};

lazy_static! {
    static ref REGEX: Regex = Regex::new(r"<(\S*?)[^>]*>.*?|<.*? />").expect("Invalid regex!");
}

// openbible.info integration
// allows using the topical bible search from The Rock
pub fn get_topic(topic: &str) -> WebResult<Vec<String>> {
    // directory for cached topics
    let topic_cache = format!("{}/bible_topics", *CACHE_DIR);

    let mut topic_f = String::new();
    for c in topic.chars() {
        if c == ' ' {
            topic_f.push('_');
        }
        else if c != '\"' {
            topic_f.push(c);
        }
    }

    // if there is a cached topic, return it instead
    if let Ok(cache) = fs::read_to_string(format!("{topic_cache}/{topic_f}")) {
        let verses: Vec<String> = cache.lines().map(str::to_string).collect();
        return Ok(verses);
    }

    // get the raw html from openbible.info
    let body = blocking::get(format!("https://www.openbible.info/topics/{topic_f}"))?.text()?;
    let lines: Vec<&str> = body.lines().collect();
    let mut verses: Vec<String> = Vec::new();
    let mut results = String::new();

    // parse the html to get the verse references
    for line in lines {
        if line.contains("bibleref") && !line.contains("style") & line.contains("a href") {
            let mut verse = REGEX.replace_all(line, "").to_string();
            verse.retain(|c| !c.is_whitespace());
            writeln!(results, "{verse}").unwrap();
            verses.push(verse);
        }
    }

    results.truncate(results.len() - 1);
    fs::create_dir(&topic_cache).unwrap_or(());
    fs::write(format!("{topic_cache}/{topic_f}"), results).expect("Cache error!");
    Ok(verses)
}