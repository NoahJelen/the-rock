use std::{
    collections::HashMap,
    env::Args,
    fs
};
use cursive::{
    align::HAlign,
    event::{Event, EventResult, Key},
    theme::{
        BaseColor,
        Color,
        ColorStyle,
        PaletteColor
    },
    utils::markup::StyledString,
    view::{
        scroll::Scroller,
        Nameable,
        Resizable,
        Scrollable
    },
    views::{
        Button,
        Dialog,
        OnEventView,
        ResizedView,
        ScrollView,
        SelectView,
        TextView,
        ViewRef
    },
    Cursive
};
use cursive_extras::*;
use lazy_static::lazy_static;
use crate::{
    CACHE_DIR,
    config::*,
    bible::{
        Book,
        BOOKS,
        Chapter,
        VerseRef
    }
};
use rust_utils::config::Config;

mod events;
mod search;

type ChapterTabs = HashMap<usize, Chapter>;
type ChapterContent = ResizedView<OnEventView<ScrollView<TextView>>>;

pub fn init(mut args: Args) {
    let saved_tabs_file = format!("{}/the_rock.tabs", *CACHE_DIR);
    let maybe_arg = args.next();
    // if there was a specific chapter at the command line, go to it
    let new_chapter = if let Some(ref arg) = maybe_arg {
        if arg == "--reset-tabs" {
            fs::remove_file(&saved_tabs_file).unwrap_or(());
            BOOKS[0].chapter(1)
        }
        else if let Some(verse_data) = VerseRef::parse(arg) {
            verse_data.chapter()
        }
        else { BOOKS[0].chapter(1) }
    }
    else { BOOKS[0].chapter(1) };

    // reopen saved tabs or go to Genesis 1
    let encoded = match fs::read(&saved_tabs_file) {
        Ok(f) => f,
        Err(_) => {
            fs::remove_file(&saved_tabs_file).unwrap_or(());
            vec![]
        }
    };

    let mut chapter_tabs = if let Ok(mut map) = bincode::deserialize::<ChapterTabs>(&encoded) {
        // insert the new chapter if there was one at the command line
        if maybe_arg.is_some() {
            let mut keys = map.keys().copied().collect::<Vec<usize>>();
            keys.sort_unstable();
            let new_key = keys.last().copied().unwrap();
            map.insert(new_key + 1, new_chapter);
        }

        for chapter in map.values_mut() {
            chapter.load_content();
        }
        map
    }
    else {
        let mut map = ChapterTabs::new();
        map.insert(0, new_chapter);
        map
    };

    let mut root = cursive::default();
    root.set_fps(30);

    // the theme of the application
    let mut theme_def = better_theme();
    theme_def.palette[PaletteColor::HighlightText] = Color::Dark(BaseColor::Black);
    theme_def.palette[PaletteColor::Highlight] = Color::Rgb(201, 157, 21);
    root.set_theme(theme_def);

    // application status
    let config = BibleConfig::load();
    let status = StatusView::new().label(format!("Verse view format: {}", config.format));

    // bible text view
    root.add_fullscreen_layer(
        vlayout!(
            hlayout!(
                fixed_hspacer(1),
                Button::new_raw(" Books ", goto_chapter),
                Button::new_raw(" Bookmarks ", events::show_bookmarks),
                Button::new_raw(" Parables ", events::show_parables),
                Button::new_raw(" Search ", search::show_search),
                Button::new_raw(" Topics ", search::show_topic_search),
                Button::new_raw(" Help ", events::show_help),
                filled_hspacer()
            ),
            get_tab_view(&mut chapter_tabs).with_name("tabs"),
            status.with_name("status")
        )
    );

    root.set_user_data(chapter_tabs);

    // exit the application
    root.add_global_callback('q', |root| {
        save_tabs(root);
        root.quit();
    });

    // toggle showing of parables
    root.add_global_callback('p', |root| {
        let mut settings = BibleConfig::load();
        let show_parables = !settings.show_parables;
        settings.show_parables = show_parables;
        settings.save().expect("Houston, we have a problem!");
        reload_chapter(root);
    });

    // toggle showing of bookmarks
    root.add_global_callback('b', |root| {
        let mut settings = BibleConfig::load();
        let show_bookmarks = !settings.show_bookmarks;
        settings.show_bookmarks = show_bookmarks;
        settings.save().expect("Houston, we have a problem!");
        reload_chapter(root);
    });

    // add a bookmark
    root.add_global_callback('a', events::add_bookmark);
    
    // toggle verse format
    root.add_global_callback('n', |root| {
        // load the config
        let mut settings: BibleConfig = BibleConfig::load();

        let mut tabs = root.find_name::<TabDialog>("tabs").unwrap();
        let mut status: ViewRef<StatusView> = root.find_name("status").unwrap();
        let chapter_tabs = root.user_data::<ChapterTabs>().unwrap();

        // set the new format
        let new_format = settings.format.next();
        settings.format = new_format;
        settings.save().expect("Houston, we have a problem!");

        for (tab_idx, view) in tabs.views_mut() {
            let tab = view.downcast_mut::<ChapterContent>().unwrap();
            let chapter = chapter_tabs.get_mut(tab_idx).unwrap();
            chapter.reformat(new_format);
            *tab = chapter_layout(chapter.content());
        }
        status.set_label(format!("Verse view format: {new_format}"));
        drop(tabs);
        save_tabs(root)
    });

    root.run();
}

// refresh the chapter text and title for the current tab
fn reload_chapter(root: &mut Cursive) {
    let mut tabs = root.find_name::<TabDialog>("tabs").unwrap();
    let cur_id = tabs.cur_id().unwrap();
    let cur_tab = tabs
        .cur_view_mut().unwrap()
        .downcast_mut::<ChapterContent>().unwrap();
    let chapter_tabs = root.user_data::<ChapterTabs>().unwrap();
    let cur_chapter = chapter_tabs.get_mut(&cur_id).unwrap();
    let config = BibleConfig::load();
    cur_chapter.reformat(config.format);
    let chapter_text = cur_tab
        .get_inner_mut()
        .get_inner_mut()
        .get_inner_mut();

    chapter_text.set_content(cur_chapter.content());
    tabs.set_title(cur_id, cur_chapter.title().as_str());
    drop(tabs);
    save_tabs(root);
}

// adds a new chapter tab
fn new_chapter_tab(root: &mut Cursive, chapter: Chapter) {
    let mut tabs: ViewRef<TabDialog> = root.find_name("tabs").unwrap();
    let new_id = tabs.add_tab(chapter.title().as_str(), chapter_layout(chapter.content()));
    let chapter_tabs = root.user_data::<ChapterTabs>().unwrap();
    chapter_tabs.insert(new_id, chapter);
    drop(tabs);
    save_tabs(root);
}

fn goto_chapter(root: &mut Cursive) {
    // book list
    let mut book_list: SelectView<Book> = SelectView::new();

    for book in BOOKS {
        book_list.add_item(book.name, book);
    }

    root.add_layer(
        OnEventView::new(
            c_focus!(
                Dialog::around(
                    book_list
                        .on_submit(|root: &mut Cursive, book| {
                            let book = *book;

                            // just go to the book if it has only 1 chapter
                            if book.single_chapter() {
                                new_chapter_tab(root, book.chapter(1));
                                root.pop_layer();
                                return;
                            }

                            // chapter list
                            let mut chapter_list: SelectView<usize> = SelectView::new();

                            for c in 1..book.chapters + 1 {
                                chapter_list.add_item(c.to_string(), c);
                            }

                            root.add_layer(
                                OnEventView::new(
                                    c_focus!(
                                        Dialog::around(
                                            chapter_list
                                                .on_submit(move |root, chapter| {
                                                    new_chapter_tab(root, book.chapter(*chapter));
                                                    root.pop_layer();
                                                    root.pop_layer();
                                                })
                                                .scrollable()
                                        )
                                            .title(format!("Select Chapter ({book})"))
                                            .dismiss_button("Back")
                                            .max_size((40, 20))
                                    )
                                )
                                    .on_event(Event::Key(Key::Esc), |r| { r.pop_layer(); })
                            );
                        })
                        .scrollable(),
                )
                    .title("Select Book")
                    .dismiss_button("Cancel")
                    .max_size((30, 12))
            )
        )
            .on_event(Event::Key(Key::Esc), |root| { root.pop_layer(); })
    );
}

fn get_tab_view(tabs: &mut ChapterTabs) -> TabDialog {
    let mut tab_dialog = TabDialog::new()
        .button("Previous Chapter", |root| {
            let tabs = root.find_name::<TabDialog>("tabs").unwrap();
            let chapter_tabs = root.user_data::<ChapterTabs>().unwrap();
            let tab_idx = tabs.cur_id().unwrap();
            chapter_tabs.get_mut(&tab_idx).unwrap().prev();
            drop(tabs);
            reload_chapter(root);
        })
        .close_button(true)
        .button("Next Chapter", |root| {
            let tabs = root.find_name::<TabDialog>("tabs").unwrap();
            let chapter_tabs = root.user_data::<ChapterTabs>().unwrap();
            let tab_idx = tabs.cur_id().unwrap();
            chapter_tabs.get_mut(&tab_idx).unwrap().next();
            drop(tabs);
            reload_chapter(root);
        })
        .button_align(HAlign::Center);

    let chapters: Vec<Chapter> = tabs.values().cloned().collect();
    tabs.clear();

    for mut chapter in chapters {
        chapter.load_content();
        let id = tab_dialog.add_tab(chapter.title().as_str(), chapter_layout(chapter.content()));
        tabs.insert(id, chapter);
    }

    tab_dialog
}

fn chapter_layout(chapter_content: StyledString) -> ChapterContent {
    use cursive::With;
    TextView::new(chapter_content)
        .scrollable()
        .wrap_with(OnEventView::new)
        // scroll up and down with the PageUp/Down keys
        .on_pre_event_inner(Key::PageUp, |root, _| {
            let scroller = root.get_scroller_mut();
            if scroller.can_scroll_up() {
                scroller.scroll_up(scroller.last_outer_size().y - 1);
            }
            Some(EventResult::consumed())
        })
        .on_pre_event_inner(Key::PageDown, |root, _| {
            let scroller = root.get_scroller_mut();
            if scroller.can_scroll_down() {
                scroller.scroll_down(scroller.last_outer_size().y - 1);
            }
            Some(EventResult::consumed())
        })
        .full_screen()
}

fn save_tabs(root: &mut Cursive) {
    let tabs = root.find_name::<TabDialog>("tabs").unwrap();
    let ids = tabs.ids();

    // save the active tab data
    let chapter_tabs = root.user_data::<ChapterTabs>().unwrap();
    chapter_tabs.retain(|id, _| { ids.contains(id) });
    let encoded = bincode::serialize(&chapter_tabs).unwrap();
    fs::write(format!("{}/the_rock.tabs", *CACHE_DIR), encoded).unwrap();
}