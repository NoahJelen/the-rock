use std::sync::RwLock;
use cursive::{
    Cursive,
    event::{Event, Key},
    theme::{BaseColor, Color, ColorStyle, Style},
    utils::markup::StyledString,
    view::{Nameable, Scrollable, Resizable},
    views::{
        Dialog,
        EditView,
        OnEventView,
        TextView,
        ViewRef
    }
};
use regex::RegexBuilder;
use crate::{
    bible::{search_word, Format},
    obi_search
};
use reqwest::Result as WebResult;
use super::*;

lazy_static! {
    static ref LAST_TOPIC: RwLock<String> = RwLock::new(String::new());
    static ref LAST_TERM: RwLock<String> = RwLock::new(String::new());
}

pub fn show_search(root: &mut Cursive) {
    load_resource(root, "Please Wait", "Loading Results...",
        || {
            let last_term = LAST_TERM.read().unwrap().to_string();
            if &last_term != "ERR" && !last_term.is_empty() {
                gen_results(&last_term)
            }
            else { vec![] }
        },

        |root, results| {
            let mut term_input = styled_editview("", "term", false);
            term_input.get_mut().set_on_submit(|v, _|{ search(v, false); });
            let last_term = LAST_TERM.read().unwrap().to_string();
            let mut results_disp: AdvancedSelectView<VerseRef> = AdvancedSelectView::new()
                .on_submit(|root, verse: &VerseRef| {
                    let chapter = verse.chapter();
                    let mut status: ViewRef<StatusView> = root.find_name("status").unwrap();
                    let mut tabs: ViewRef<TabDialog> = root.find_name("tabs").unwrap();
                    let term: ViewRef<EditView> = root.find_name("term").unwrap();
                    let case = get_checkbox_option(root, "case");
                    let chapter_tabs = root.user_data::<ChapterTabs>().unwrap();
                    let regex = report_error!(status,
                        RegexBuilder::new(&term.get_content())
                            .case_insensitive(!case)
                            .build()
                    );
                    let new_id = tabs.add_tab(chapter.title().as_str(), chapter_layout(chapter.content_with_results(regex)));
                    chapter_tabs.insert(new_id, chapter);
                    drop(tabs);
                    save_tabs(root);
                    root.pop_layer();
                });

            term_input.get_mut().set_content(&last_term);
            for (verse, content) in results {
                results_disp.add_item(content, verse);
            }

            let layout = vlayout!(
                TextView::new("Search Term:\n"),
                term_input,
                labeled_checkbox("Case sensitive", "case", false),
                results_disp.with_name("results").scrollable()
            )
                .max_size((70, 100));

            root.add_layer(
                OnEventView::new(
                    Dialog::around(layout)
                        .dismiss_button("Back")
                        .button("Search", |v| search(v, false))
                        .button("Reset", |v| reset(v, false))
                        .title("Text Search")
                )
                    .on_event(Event::Key(Key::Esc), |root| { root.pop_layer(); })
            );
        }
    );
}

pub fn show_topic_search(root: &mut Cursive) {
    load_resource(root, "Please Wait", "Loading Results...",
        move || {
            let last_term = LAST_TOPIC.read().unwrap();
            if !last_term.is_empty() {
                gen_topic_results(&last_term)
            }
            else {
                Ok(vec![])
            }
        },

        move |root, results| {
            let mut status: ViewRef<StatusView> = root.find_name("status").unwrap();
            let results = report_error!(status, results);
            let input_style = ColorStyle::new(
                Color::Light(BaseColor::White),
                Color::Dark(BaseColor::Blue)
            );
            let last_term = LAST_TOPIC.read().unwrap();
            let mut term_input = EditView::new().style(input_style).filler(" ")
                .on_submit(|v, _| search(v, true));

            term_input.set_content(&*last_term);
            let mut results_disp: AdvancedSelectView<VerseRef> = AdvancedSelectView::default()
                .on_submit(|root, verse: &VerseRef| {
                    let chapter = verse.chapter();
                    let mut tabs: ViewRef<TabDialog> = root.find_name("tabs").unwrap();
                    let term: ViewRef<EditView> = root.find_name("term").expect("Houston, we have a problem!");
                    let chapter_tabs = root.user_data::<ChapterTabs>().unwrap();
                    let new_id = tabs.add_tab(chapter.title().as_str(), chapter_layout(chapter.content_with_topic(&term.get_content(), verse)));
                    chapter_tabs.insert(new_id, chapter);
                    drop(tabs);
                    save_tabs(root);
                    root.pop_layer();
                });

            for (verse, content) in results {
                results_disp.add_item(content, verse);
            }

            let layout = vlayout!(
                TextView::new("What the Bible says about:\n"),
                term_input.with_name("term"),
                results_disp.with_name("results").scrollable(),
                TextView::new("\nPowered by openbible.info")
            )
                .max_size((70, 100));
        
            root.add_layer(
                OnEventView::new(
                    Dialog::around(layout)
                        .dismiss_button("Back")
                        .button("Search", |v| search(v, true))
                        .button("Reset", |v| reset(v, true))
                        .title("Topic Search")
                )
                    .on_event(Event::Key(Key::Esc), |s| { s.pop_layer(); })
            );
        }
    );
}

fn gen_results(term: &str) -> Vec<(VerseRef, StyledString)> {
    let regex = RegexBuilder::new(term)
        .case_insensitive(true)
        .build()
        .expect("Invalid Regex!");

    let mut results_fmt = Vec::new();
    let results = search_word(&regex);

    for verse in results {
        if let Some(book) = Book::by_name(verse.book) {
            let mut disp = StyledString::styled(format!("{book} {}:{}: ", verse.chapter, verse.verse), Style::from(Color::Light(BaseColor::White)));
            disp.append(verse.content_with_results(&regex));
            results_fmt.push((verse, disp));
        }
    }

    *(LAST_TERM.write().unwrap()) = term.to_string();
    results_fmt
}

fn gen_topic_results(term: &str) -> WebResult<Vec<(VerseRef, StyledString)>> {
    *(LAST_TOPIC.write().unwrap()) = term.to_string();
    let results = obi_search::get_topic(term)?;
    let mut results_fmt = Vec::new();
    for verse in results {
        if let Some(verse_data) = VerseRef::parse(verse) {
            let mut disp = StyledString::new();
            let verse = verse_data.format(Format::Paragraph);
            disp.append(StyledString::styled(verse.title(), Style::from(Color::Light(BaseColor::White))));
            disp.append(StyledString::styled(verse, Style::from(Color::Dark(BaseColor::White))));
            results_fmt.push((verse_data, disp));
        }
    }

    Ok(results_fmt)
}

fn reset(root: &mut Cursive, is_topic_search: bool) {
    if is_topic_search{
        *(LAST_TOPIC.write().unwrap()) = String::new();
    }
    else {
        *(LAST_TERM.write().unwrap()) = String::new();
    }
    let mut results_view: ViewRef<AdvancedSelectView<VerseRef>> = root.find_name("results").expect("Houston, we have a problem!");
    results_view.clear();
    let mut term: ViewRef<EditView> = root.find_name("term").expect("Houston, we have a problem!");
    term.set_content("");
}

fn search(root: &mut Cursive, is_topic: bool) {
    let term: ViewRef<EditView> = root.find_name("term").expect("Houston, we have a problem!");
    let content = term.get_content().to_string();
    load_resource(root, "Please Wait", "Loading Results...",
        move || {
            if is_topic {
                gen_topic_results(&content)
            }
            else {
                Ok(gen_results(&content))
            }
        },
        |root, results| {
            let mut status: ViewRef<StatusView> = root.find_name("status").unwrap();
            let results = report_error!(status, results);
            let mut results_view: ViewRef<AdvancedSelectView<VerseRef>> = root.find_name("results").expect("Houston, we have a problem!");
            results_view.clear();
            for (verse, content) in results {
                results_view.add_item(content, verse);
            }
        }
    )
}