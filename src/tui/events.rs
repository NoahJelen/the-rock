use std::cell::RefCell;
use cursive::{
    theme::Style,
    views::EditView
};
use crate::{
    bible::PARABLES,
    VERSION
};
use rust_utils::config::Config;

use super::*;

thread_local! {
    static BOOKMARK_TO_DEL: RefCell<String> = RefCell::new(String::new());
}

// show the help window
pub fn show_help(root: &mut Cursive) {
    let parable_style = Style::from(Color::Light(BaseColor::Red));
    let bookmark_style = Style::from(Color::Light(BaseColor::Blue));
    let topic_style = Style::from(Color::from_256colors(165));
    let parable_title_style = ColorStyle::new(Color::Light(BaseColor::White), Color::Dark(BaseColor::Red));
    let bookmark_title_style = ColorStyle::new(Color::Light(BaseColor::White), Color::Dark(BaseColor::Blue));
    let topic_title_style = ColorStyle::new(Color::Light(BaseColor::White), Color::from_256colors(165));
    let mut annotation_legend = StyledString::plain("Annotation Legend:\n");
    annotation_legend.append(StyledString::styled("Parable Title\n", parable_title_style));
    annotation_legend.append(StyledString::styled("parable text\n\n", parable_style));
    annotation_legend.append(StyledString::styled("Bookmark Title\n", bookmark_title_style));
    annotation_legend.append(StyledString::styled("bookmark text\n\n", bookmark_style));
    annotation_legend.append(StyledString::styled("Topic Verse\n", topic_title_style));
    annotation_legend.append(StyledString::styled("topic verse\n", topic_style));
    let mut help_text = StyledString::plain(format!(
        "The Rock: A King James Bible Viewer\n\
        Version {VERSION}\n\
        \n\
        Key Shortcuts:\n\
        q: Quit\n\
        n: set verse view format\n\
        a: Add verses in current chapter as named bookmark\n\
        b: Show/Hide Bookmarks\n\
        p: Show/Hide Parables\n\
        \n"
    ));
    help_text.append(annotation_legend);
    root.add_layer(info_dialog("Help", help_text))
}

// show the parables window
pub fn show_parables(root: &mut Cursive) {
    let mut parables_list: AdvancedSelectView<VerseRef> = AdvancedSelectView::new();
    for (title, verse) in PARABLES.iter().copied() {
        parables_list.add_item(title, VerseRef::parse(verse).unwrap())
    }
    root.add_layer(
        OnEventView::new(
            c_focus!(
                Dialog::around(
                    parables_list
                        .on_submit(|view, verse| {
                            new_chapter_tab(view, verse.chapter());
                            view.pop_layer();
                        })
                        .scrollable()
                )
                    .title("Parables")
                    .dismiss_button("Back")
            )
        )
            .on_event(Event::Key(Key::Esc), |root| { root.pop_layer(); })
            .max_size((40, 20))
    )
}

// show the bookmarks window
pub fn show_bookmarks(root: &mut Cursive) {
    let settings = BibleConfig::load();
    let bookmarks = settings.bookmarks;
    let mut bookmark_list: AdvancedSelectView<VerseRef> = AdvancedSelectView::new();
    for (title, verse_ref) in bookmarks {
        if let Some(verse) = VerseRef::parse(verse_ref) {
            bookmark_list.add_item(title, verse);
        }
    }
    root.add_layer(
        OnEventView::new(
            c_focus!(
                Dialog::around(
                    bookmark_list
                        .on_select(|_, verse| {
                            BOOKMARK_TO_DEL.with(move |bm| *(bm.borrow_mut()) = verse.title());
                        })
                        .on_submit(|root, verse | {
                            new_chapter_tab(root, verse.chapter());
                            root.pop_layer();
                        })
                        .scrollable()
                )
                    .dismiss_button("Back")
                    .title("Bookmarks")
            )
        )
            .on_event(Event::Key(Key::Esc), |root| { root.pop_layer(); })
            .on_event(Event::Key(Key::Del), |root| {
                root.add_layer(
                    confirm_dialog("Delete Bookmark", "Are you sure?", |root| {
                        let mut status: ViewRef<StatusView> = root.find_name("status").unwrap();
                        let mut settings = BibleConfig::load();
                        let bookmark_to_delete = BOOKMARK_TO_DEL.with(|bm|bm.borrow().clone());
                        settings.bookmarks.retain(|(_, verse)| bookmark_to_delete != *verse);
                        report_error!(status, settings.save());
                        status.info("Bookmark deleted");
                        root.pop_layer();
                        root.pop_layer();
                        reload_chapter(root);
                    })
                )   
            })
            .with_name("bookmark_view")
            .max_size((40, 20))
    );
}

// add a new bookmark
pub fn add_bookmark(root: &mut Cursive) {
    let tabs = root.find_name::<TabDialog>("tabs").unwrap();
    let cur_id = tabs.cur_id().unwrap();
    let chapter_tabs = root.user_data::<ChapterTabs>().unwrap();
    let cur_chapter = &chapter_tabs[&cur_id];
    let book = cur_chapter.book;
    let chapter_num = cur_chapter.num.to_string();

    root.add_layer(
        settings!(
            "Add Bookmark",
            move |root| {
                let mut status: ViewRef<StatusView> = root.find_name("status").unwrap();
                let mut settings = BibleConfig::load();
                let title = root.find_name::<EditView>("title").unwrap().get_content();
                let book = {
                    let name = root.find_name::<EditView>("book").unwrap().get_content().to_string();
                    if let Some(book) = Book::by_name(name) {
                        book.short2
                    }
                    else {
                        root.add_layer(info_dialog("Error", "Invalid book name!"));
                        return;
                    }
                };
                let chapter_num_str = root.find_name::<EditView>("chapter_num").unwrap().get_content();
                let verse_num_str = root.find_name::<EditView>("verse_num").unwrap().get_content();
                let last_verse_num_str = root.find_name::<EditView>("last_verse_num").unwrap().get_content();
                let verse = if last_verse_num_str.as_str() != "0" {
                    format!("{book}{chapter_num_str}:{verse_num_str}-{last_verse_num_str}")
                }
                else {
                    format!("{book}{chapter_num_str}:{verse_num_str}")
                };
                settings.bookmarks.push((title.to_string(), verse));
                report_error!(status, settings.save());
                status.info("Bookmark added");
                root.pop_layer();
                super::reload_chapter(root);
            },
            TextView::new("Title:"),
            styled_editview("", "title", false),
            TextView::new("Book:"),
            styled_editview(book, "book", false),
            TextView::new("Chapter:"),
            styled_editview(chapter_num, "chapter_num", false),
            TextView::new("Verse:"),
            styled_editview("","verse_num", false),
            TextView::new("Ending verse:"),
            styled_editview("0","last_verse_num", false),
            TextView::new("^ Leave this at 0 for single verse")
        )
    )
}