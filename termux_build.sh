#!/system/bin/sh
cargo clean
cargo build --release
mkdir $PREFIX/share/the_rock
cp bible.rawtext $PREFIX/share/the_rock/bible.rawtext
cp icon.png $PREFIX/share/the_rock/icon.png
cp target/release/the_rock $PREFIX/bin/the_rock
ln -sf $PREFIX/bin/the_rock $PREFIX/bin/bible
cp the_rock.desktop $PREFIX/share/applications/the_rock.desktop

cp man/the_rock.1 $PREFIX/share/man/man1/the_rock.1
cp man/bible.1  $PREFIX/share/man/man1/bible.1
gzip $PREFIX/share/man/man1/bible.1
gzip $PREFIX/share/man/man1/the_rock.1
echo "The Rock has been installed on your system"