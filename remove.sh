#!/bin/sh
sudo rm -rf /usr/lib/the_rock
sudo rm -rf /usr/share/the_rock
sudo rm /usr/bin/bible
sudo rm /usr/bin/the_rock
sudo rm /usr/share/applications/the_rock.desktop
sudo rm /usr/share/man/man1/bible.1.gz
sudo rm /usr/share/man/man1/the_rock.1.gz
echo "The Rock has been removed from your system"
