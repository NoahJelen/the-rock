use std::{fs, env};
use rust_utils::linux::{
    DesktopEntryFileBuilder,
    ManpageBuilder,
    AurPkgbuildBuilder
};

fn main() {
    println!("cargo:rerun-if-changed=build.rs");

    if fs::read_dir("man/").is_err() {
        fs::create_dir("man/").unwrap();
    }

    let version = env!("CARGO_PKG_VERSION");

    let icon_dir = if fs::read_dir("/storage/emulated/0").is_ok() {
        format!("{}/share/the_rock/icon.png", env::var("PREFIX").unwrap())
    }
    else {
        "/usr/share/the_rock/icon.png".to_string()
    };

    DesktopEntryFileBuilder::new("The Rock", "the_rock", true, version)
        .comment("Command line King James bible viewer")
        .icon(&icon_dir)
        .category("Utility")
        .build("the_rock.desktop");

    ManpageBuilder::new(
        "the_rock",
        "Command line King James bible viewer",
        "Noah Jelen",
        "noahtjelen@gmail.com",
        "the_rock [chapter]\n\
        The Rock is a command line program to view the King James version of the bible"
    )
        .section(
            "OPTIONS",
            "If you specify a specific chapter as an argument, or if none are specified, The Rock will open the last chapter it closed on, or it will open Genesis 1\n\
            Pass --reset-tabs to close all open chapter tabs"
        )
        .other_page("bible")
        .build(version, "man/the_rock.1");

    ManpageBuilder::new(
        "bible",
        "King James bible verse reference utility",
        "Noah Jelen",
        "noahtjelen@gmail.com",
        "bible is the command line interface of The Rock."
    )
        .section(
            "SYNOPSIS",
            "bible <verse>\n\
            bible -p <verse>\n\
            bible -c <chapter>\n\
            bible -s <search term>\n\
            bible <passage>\n\
            bible -t <topic>\n\
            bible [verse or passage][,verse or passage,verse or passage...]"
        )
        .section(
            "OPTIONS", 
            "If no options are specifed, bible will just show Genesis 1:1\n\
            Use -v to show current version\n\
            Use -h to show the help message\n\
            Use -p <passage> to show the passage as a paragraph instead of verse list\n\
            Use -c <book number><chapter number> to show an entire chapter\n\
            Use -s <word> to find all instances of a specific word"
        )
        .section(
            "EXAMPLES",
            "bible Mark9:23\n\
            bible matt24:4-14\n\
            bible isa40:31,matt6:9-13,Genesis1\n\
            bible -p matt24:4-14\n\
            bible -c genesis1\n\
            bible -cp exodus20"
        )
        .other_page("the_rock")
        .build(version, "man/bible.1");

    AurPkgbuildBuilder::new(
        "the-rock",
        version,
        "Noah Jelen",
        "noahtjelen@gmail.com",
        "A command line King James bible viewer",
        "https://gitlab.com/NoahJelen/the-rock/-/archive/$pkgver/the-rock-$pkgver.zip",
        "https://gitlab.com/NoahJelen/the-rock",
        "GPL-3",
        "cd \"the-rock-$pkgver\"\n\
        cargo build --release\n\
        cd target/release\n\
        ln -sf the_rock bible",
        "cd \"the-rock-$pkgver\"\n\
        mkdir -p \"$pkgdir/usr/share/the_rock\"\n\
        mkdir -p \"$pkgdir/usr/share/man/man1/\"\n\
        install -Dt \"$pkgdir/usr/bin\" -m755 target/release/the_rock\n\
        install -Dt \"$pkgdir/usr/bin\" -m755 target/release/bible\n\
        install -Dt \"$pkgdir/usr/share/the_rock/\" bible.rawtext\n\
        install -Dt \"$pkgdir/usr/share/applications/\" the_rock.desktop\n\
        install -Dt \"$pkgdir/usr/share/the_rock/\" icon.png\n\
        install -Dt \"$pkgdir/usr/share/man/man1\" man/the_rock.1\n\
        install -Dt \"$pkgdir/usr/share/man/man1\" man/bible.1"
    )
        .dependency("gcc-libs")
        .dependency("glibc")
        .make_dependency("cargo")
        .build("aur/");
}