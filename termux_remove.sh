#!/system/bin/sh
rm -rf $PREFIX/lib/the_rock
rm -rf $PREFIX/share/the_rock
rm $PREFIX/bin/bible
rm $PREFIX/bin/the_rock
rm $PREFIX/share/applications/the_rock.desktop
rm $PREFIX/share/man/man1/bible.1.gz
rm $PREFIX/share/man/man1/the_rock.1.gz
echo "The Rock has been removed from your system"