#!/bin/sh
cargo build --release
sudo mkdir /usr/share/the_rock
sudo cp bible.rawtext /usr/share/the_rock/bible.rawtext
sudo cp icon.png /usr/share/the_rock/icon.png
sudo cp target/release/the_rock /usr/bin/the_rock
sudo chmod 755 /usr/bin/the_rock
sudo ln -sf /usr/bin/the_rock /usr/bin/bible
sudo cp the_rock.desktop /usr/share/applications/the_rock.desktop

sudo install -Dt /usr/share/man/man1 man/the_rock.1
sudo install -Dt /usr/share/man/man1 man/bible.1
sudo gzip /usr/share/man/man1/bible.1
sudo gzip /usr/share/man/man1/the_rock.1
echo "The Rock has been installed on your system"