[![Crates.io](https://img.shields.io/crates/v/the_rock)](https://crates.io/crates/the_rock)
[![AUR version](https://img.shields.io/aur/version/the-rock)](https://aur.archlinux.org/packages/the-rock/)

# The Rock
![](icon.png)

The Rock is a command-line application for Linux systems very similar to that of Debian's `bible-kjv` application, but with extra features, like being able to actually read the bible (`bible-kjv` is only good for making references to bible passages). This application is under heavy development and is ***NOT*** feature complete yet. What you see in the application now can and will change in the near future.

# Screenshots

## Command line mode. Just like Debian's `bible-kjv`, this is good for referencing bible passages and verses.
![](screenshots/command_line.png)


## TUI (Terminal UI) mode, this is good for wanting to read the bible. It can show the chapters with the verse numbers, show each chapter as a paragraph, or show the chapter as a paragraph with each verse numbered. (press the 'n' key to set this)
![](screenshots/tui.png)


## You can bookmark your favorite passages to look at again later with labeled bookmarks!
![](screenshots/bookmarks.png)


## Creating a new bookmark
![](screenshots/new_bookmark.png)


## All of Jesus Christ's parables, easily accessible.
![](screenshots/parables.png)

## Text Search
![](screenshots/text_search.png)

## Find out what the Bible says about things with openbible.info topic search
![](screenshots/obi_topic_search.png)

# How to install

## Arch Linux based Linux systems
The Rock is available in the AUR for Arch Linux and any system based on it (like Manjaro Linux, EndeavourOS, and Artix Linux)

Installation example using `yay`: `yay -S the-rock`

## Other Linux based systems

Make sure you have the latest version of Rust installed

Instructions on how to install it are [here](https://rustup.rs/)

### Debian and systems based on it (Like Devuan, Ubuntu, and Linux Mint)
Make sure you install the libncurses5-dev and libncursesw5-dev packages before building.

### Manually

After installing Rust run the following commands:

`git clone https://gitlab.com/NoahJelen/the-rock`

`cd the-rock`

`./build.sh`  <-- This will request root access in order to install the program

To remove: run `./remove.sh`

To do:
- [ ] Custom colored bookmarks
- [ ] Section titles (I'll have to get these from a physical KJV bible)
- [ ] Make verses selectable for bookmarking instead of manually inputting the verse numbers
- [ ] GUI Support
- [ ] Add a vim-like command pane
- [ ] Allow copying of verses to the clipboard for easy sharing
- [ ] Allow users to download other bible versions from Bible Gateway, Bible Study Tools, and the SWORD API
- [x] Tabbed interface to allow opening of multiple chapters
- [x] King James bible support
- [x] Support for Termux on Android
- [x] Add paragraph with verse numbers formatting option
- [x] Open the program on the last chapter it was closed on
- [x] Add a status bar
- [x] openbible.info integration (will making studying the bible way easier)
